variable "aws_access_key" {
  default = "<AWS_ACCESS_KEY>"
}

variable "aws_secret_key" {
  default = "<AWS_SECRET_KEY>"
}

variable "aws_region" {
}

variable "availabilityZone" {
	default = "us-east-1a"
}

variable "instanceTenancy" {
	default = "default"
}

variable "dnsSupport" {
	default = true
}

variable "associatePUBLICip" {
	default = true
}

variable "dnsHostNames" {
	default = true
}

variable "vpcCIDRblock" {
	default = "10.0.0.0/16"
}

variable "subnetCIDRblock" {
	default = "10.0.1.0/24"
}

variable "destinationCIDRblock" {
	default = "0.0.0.0/0"
}

variable "ingressCIDRblock" {
	type = "list"
    default = [ "0.0.0.0/0" ]
}

variable "ingressCIDRblockSsh" {
	default = [ "0.0.0.0/0" ]
}


variable "mapPublicIP" {
    default = true
}

variable "publicKey" {
	default = "CHAVE PUBLICA"
}