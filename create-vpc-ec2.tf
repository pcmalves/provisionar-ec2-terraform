provider "aws" {
	access_key = "${var.aws_access_key}"
	secret_key = "${var.aws_secret_key}"
	region = "${var.aws_region}"
}

resource "aws_vpc" "VPC_TESTE" {
  cidr_block           = "${var.vpcCIDRblock}"
  instance_tenancy     = "${var.instanceTenancy}" 
  enable_dns_support   = "${var.dnsSupport}" 
  enable_dns_hostnames = "${var.dnsHostNames}"
}

resource "aws_subnet" "VPC_TESTE_SUBNET" {
  vpc_id                  = "${aws_vpc.VPC_TESTE.id}"
  cidr_block              = "${var.subnetCIDRblock}"
  map_public_ip_on_launch = "${var.mapPublicIP}" 
  availability_zone       = "${var.availabilityZone}"
}

resource "aws_route_table" "VPC_TESTE_ROUTE_TABLE" {
    vpc_id = "${aws_vpc.VPC_TESTE.id}"
}

resource "aws_internet_gateway" "VPC_TESTE_GATEWAY" {
  vpc_id = "${aws_vpc.VPC_TESTE.id}"
}

resource "aws_route" "VPC_TESTE_INTERNET_ACCESS" {
  route_table_id        = "${aws_route_table.VPC_TESTE_ROUTE_TABLE.id}"
  destination_cidr_block = "${var.destinationCIDRblock}"
  gateway_id             = "${aws_internet_gateway.VPC_TESTE_GATEWAY.id}"
}

resource "aws_route_table_association" "VPC_TESTE_ASSOCIATION" {
    subnet_id      = "${aws_subnet.VPC_TESTE_SUBNET.id}"
    route_table_id = "${aws_route_table.VPC_TESTE_ROUTE_TABLE.id}"
}

resource "aws_security_group" "VPC_TESTE_SECURITY_GROUP" {
  vpc_id       = "${aws_vpc.VPC_TESTE.id}"
  name         = "VPC TESTE SECURITY GROUP"
  description  = "VPC TESTE SECURITY GROUP"
ingress {
    cidr_blocks = "${var.ingressCIDRblock}"  
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    description	= "ALLOW_HTTP"
  }
ingress {
    cidr_blocks = "${var.ingressCIDRblock}"  
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    description	= "ALLOW_HTTPS"
  }
ingress {
    cidr_blocks = "${var.ingressCIDRblockSsh}"  
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    description	= "ALLOW_SSH"
  }
egress {
	cidr_blocks = ["${var.destinationCIDRblock}"]
	from_port	= 0
    to_port		= 0
    protocol   = "-1"   
    description	= "ALLOW_ALL_TRAFFFIC_OUTBOUND"
  }   
}

resource "aws_key_pair" "useracess" {
  key_name   = "useracess-key"
  public_key = "${var.publicKey}"
}

output "seu ip público" {
	value = "${aws_instance.ec2teste.public_ip}"
}

resource "aws_instance" "ec2teste" {
	ami			= "ami-0cfee17793b08a293"
	instance_type	= "t2.micro"
	security_groups	= ["${aws_security_group.VPC_TESTE_SECURITY_GROUP.id}"]
	subnet_id	= "${aws_subnet.VPC_TESTE_SUBNET.id}"
	associate_public_ip_address	= "${var.associatePUBLICip}"
	key_name	= "useracess-key"
	
	provisioner "remote-exec" {
    	inline = [
      	"sudo apt-get -y update",
      	"sudo apt-get -y upgrade",
      	"sudo apt-get -y install apt-transport-https ca-certificates",
      	"sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -",
      	"sudo sh -c 'echo \"deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable\" > /etc/apt/sources.list.d/docker.list'",
      	"sudo apt-get -y update",
      	"sudo apt-get install -y docker-ce",
      	"sudo docker pull httpd",
      	"sudo docker run -d -p 80:80 httpd:latest",
    	]

    connection {
    	type = "ssh"
		user = "ubuntu"
		private_key = "${file("~/.ssh/id_rsa")}"
  	}
  }
}