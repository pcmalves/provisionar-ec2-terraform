# Provisionar ec2 com terraform

Provisionar um instância ec2 na aws utilizando terraform para tal finalidade

O que este projeto ajuda?
-------------------------

A função deste projeto é auxiliar o provisionamento de ambientes, em uma hospedagem cloud. Pode ser utilizado com diversos provedores. Para este exemplo, utilizei a AWS. Uma exemplo clássico que este projeto pode auxiliar, por algum motivo de uma região da aws ficar indisponível. Podemos subir uma nova instância, ou até mesmo mais de 1 em outras zonas ou em outras regiões. Inicialmente criei um rede vpc para na sequencia criar um instância ec2.

Para utilização da aplicação
----------------------------
Primeiramente é necessário instalação do Terraform. E na sequência utilizar os comandos abaixo:

Iniciar o terraform

```bash
$ terraform init
```
Para verificar se está tudo ok

```bash
$ terraform plan
```
Aplicar as configurações

```bash
$ terraform apply
```
